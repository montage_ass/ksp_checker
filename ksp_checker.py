import sys
import os
import re
import configparser
import tkinter
import threading
import queue
from datetime import datetime, date, time
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.utils import get_column_letter,column_index_from_string
from openpyxl.styles import PatternFill
from copy import copy
from tkinter import Grid,Frame,Menu,scrolledtext,ttk
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import showerror,showinfo


# KSP Checker V1.2 10.10.2019 // Sabri Ramadan
# Externe Konfigurationsdatei 'ksp_options.ini' mit BT-Nummer-Spalte, Startreihe und Startspalte (und ggf. Sonderregelungen)
# Kompilierung: pyinstaller --onedir --windowed --icon="icon.ico" ksp_checker.py


version = "V1.2"


class GuiPart:
    def __init__(self, window, queue, endCommand):
        """ Aufbau des Fensters mit Steuerelementen """
		# Definiere globale GUI-Elemente
        global logentry
        global btn1
        global btn2
        global excel_file_label
        global bt_col_input
        global bt_row_input
        global compare_col_input
        global progress
        
        global chkbox1_value
        global chkbox2_value
        global radio_value
        
		# Queue aus ThreadedClient
        self.queue = queue
        
        # Hinzugefügt, damit "Schließen"-Button auch threads beendet
        window.protocol("WM_DELETE_WINDOW", endCommand)
        
        window.title("KSP Checker " + version)
        Grid.rowconfigure(window, 0, weight=1)
        Grid.columnconfigure(window, 0, weight=1)
	
		# Größe des Fensters: 1/3 der Bildschirmbreite, 1/2 der Bildschirmhöhe, zentriert in der Mitte des Screens
        user_screen_width05 = int(window.winfo_screenwidth() * (1/3))
        user_screen_height05 = int(window.winfo_screenheight() * 0.5)
        user_screen_offsetx = int(window.winfo_screenwidth() * (1/3))
        user_screen_offsety = int(window.winfo_screenheight() * 0.25)
        sizestring = "" + str(user_screen_width05) + "x" + str(user_screen_height05) + "+" + str(user_screen_offsetx) + "+" + str(user_screen_offsety)
        window.geometry(sizestring)

        #print(window.winfo_screenwidth(), window.winfo_screenheight())

        # Frame erzeugen & konfigurieren
        frame=Frame(window)
        frame.grid(row=0, column=0, sticky='nsew')
        
        # Menüzeile
        menu = Menu(window)
        window.config(menu=menu)
        optionsmenu = Menu(menu, tearoff=0)
        scalingmenu = Menu(menu, tearoff=0)
        menu.add_cascade(label="Einstellungen", menu=optionsmenu)
        
        radio_value = tkinter.IntVar()
        scalingmenu.add_radiobutton(label="100% (Standard)", value=100, variable=radio_value, command=self.adjust_scaling)
        scalingmenu.add_radiobutton(label="125%", value=125, variable=radio_value, command=self.adjust_scaling)
        scalingmenu.add_radiobutton(label="150%", value=150, variable=radio_value, command=self.adjust_scaling)
        scalingmenu.add_radiobutton(label="175%", value=175, variable=radio_value, command=self.adjust_scaling)
        optionsmenu.add_cascade(label="Skalierung", menu=scalingmenu)
        optionsmenu.add_separator()
        optionsmenu.add_command(label="Über...", command=self.about_menu)
        
        # DEBUG - in config erledigen
        #radio_value.set(100)
        
        # Erste Reihe des grids
        tkinter.Label(frame, text = "Excel-Ausleitung: ").grid(row = 0, column = 0, padx = 20, sticky='sw')
        
        # Zweite Reihe des grids
        excel_file_label = tkinter.Label(frame, text = "C:/...", anchor='w', bg='#eaeaea', borderwidth=1, relief="solid")
        excel_file_label.grid(row = 1, column = 0, columnspan = 2, padx = (20,0), pady = (0,20), sticky='nsew')
        btn1 = tkinter.Button(frame, text = "Öffnen", command = self.load_xlsx)
        btn1.grid(row = 1, column = 2, padx = 20, pady = (0,20), sticky='nsew')

        # Dritte Reihe des grids
        tkinter.Label(frame, text = "Spalte für BT-Nummern: ").grid(row = 2, column = 0, padx = 20, sticky='nsw')
        bt_col_input = tkinter.Entry(frame)
        bt_col_input.grid(row = 2, column = 1, sticky='nsew')
        tkinter.Label(frame, text = "z.B. K", anchor='w').grid(row = 2, column = 2, padx = 20, sticky='nsw')

        # Vierte Reihe des grids
        tkinter.Label(frame, text = "Reihe in der erste BT-Nummer steht: ", anchor='w').grid(row = 3, column = 0, padx = 20, sticky='nsw')
        bt_row_input = tkinter.Entry(frame)
        bt_row_input.grid(row = 3, column = 1, sticky='nsew')
        tkinter.Label(frame, text = "z.B. 3", anchor='w').grid(row = 3, column = 2, padx = 20, sticky='nsw')

        # Fünfte Reihe des grids
        tkinter.Label(frame, text = "Spalte ab der nach rechts verglichen werden soll: ", anchor='w').grid(row = 4, column = 0, padx = 20, sticky='nsw')
        compare_col_input = tkinter.Entry(frame)
        compare_col_input.grid(row = 4, column = 1, sticky='nsew')
        tkinter.Label(frame, text = "z.B. AW", anchor='w').grid(row = 4, column = 2, padx = 20, sticky='nsew')

        # Sechste Reihe des grids
        btn2 = tkinter.Button(frame, text = "Start", command = self.start_button)
        btn2.grid(row = 5, column = 0, rowspan = 2, padx = 20, pady = (20,10), sticky='nsew')
        btn2.config(state='disabled')
        chkbox1_value = tkinter.BooleanVar()
        chkbox1_value.set(True)
        chkbox1 = tkinter.Checkbutton(frame, text = "Excel-Datei nach Durchlauf öffnen", variable = chkbox1_value)
        chkbox1.grid(row = 5, column = 1, padx = 20, pady = (5,0), columnspan = 2, sticky='sw')
        chkbox2_value = tkinter.BooleanVar()
        chkbox2_value.set(True)
        chkbox2 = tkinter.Checkbutton(frame, text = "SAs in Strings umwandeln", variable = chkbox2_value)
        chkbox2.grid(row = 6, column = 1, padx = 20, pady = (0,5), columnspan = 2, sticky='nw')

        # Siebte Reihe des grids
        progress = ttk.Progressbar(frame, orient="horizontal", mode="determinate")
        progress.grid(row=7, columnspan = 3, padx = 20, sticky='ew')
        progress["maximum"] = 7
        progress["value"] = 0
        
        # Achte Reihe des grids
		# Höhe der Textbox: screen height 1080 -> user screen height 540 -> offset y 270 -> logbox height 13,5 --> cast to int = 13
        logentry_height = int(user_screen_offsety / 20)
        #print("Logbox Height: " + str(logentry_height))
        logentry = scrolledtext.ScrolledText(frame, height = logentry_height)
        self.log_message("KSP Checker zur Überprüfung von KSP Konfigurationen")
        self.log_message("+++ Zu überprüfendes Arbeitsblatt muss aktiv sein bei Speicherung")
        self.log_message("+++ Excel-Datei darf während der Ausführung nicht geöffnet sein")
        self.log_message("")
        # Stretch: north south east west = nsew
        logentry.grid(row = 8, columnspan = 3, padx = 20, pady = (10,20), sticky='nsew')


        for row_index in range(9):
            Grid.rowconfigure(frame, row_index, weight=1)

        for col_index in range(3):
            Grid.columnconfigure(frame, col_index, weight=1)
           
		# Optionsdatei einlesen
        self.import_config()
        self.adjust_scaling()

    def processIncoming(self):
        """ Alle Nachrichten in der Queue behandeln, falls vorhanden """
        while self.queue.qsize():
            try:
                msg = self.queue.get(0)
                # Unterscheiden zwischen String und Änderung von Button-Status
                # GUI Elemente sollten nur hier verändert werden
                if msg == "btn_enable":
                    global btn1
                    global btn2
                    btn1.config(state='normal')
                    btn2.config(state='normal')
                elif msg == "progress":
                    progress["value"] += 1
                elif msg == "progress_reset":
                    progress["value"] = 0
                else:
                    self.log_message(msg)
            except Queue.Empty:
                pass

    def log_message(self, message):
        """ Textlog befüllen """
        global logentry
        logentry.configure(state='normal')
        logentry.insert('end',message + "\n")
        logentry.yview_pickplace('end')
        logentry.configure(state='disabled')
    
    def load_xlsx(self):
        """ Excel-Ausleitung laden """
        global workbook
        global btn2
        global excel_file_label
        # Zurücksetzen von workbook Variable, "Start"-Button & Dateinamen-Label
        workbook = None
        btn2.config(state='disabled')
        excel_file_label.config(text="C:/...")
        # Evtl noch zu erweitern auf weitere Excel-Dateitypen
        workbook = askopenfilename(filetypes=[("Excel-Dateien", "*.xlsx")])
        # Schreibe Dateinamen in Label
        if workbook:
            xl_filename = str(workbook).split("/")[-1]
            drive_letter = str(workbook)[0:2] + "/... /"
            excel_file_label.config(text=drive_letter + xl_filename)
            self.log_message("Excel-Datei " + xl_filename + " geladen.")
            btn2.config(state='normal')

    
    def start_button(self):
        """ Start-Button Klick-Event """
        global btn1
        global btn2
        btn1.config(state='disable')
        btn2.config(state='disable')
        # progressbar 1
        progress["value"] = 1
        self.log_message("")
        self.log_message("+++ KSP-Checker gestartet +++")
        self.log_message("")
        
        client.running = 1
        client.periodicCall()
        client.thread1 = threading.Thread(target=client.workerThread1)
        client.thread1.start()
    
    def about_menu(self):
        """ Menü mit Informationen zum Programm """
        showinfo("Über","KSP Checker, Version: " + version + "\n" + "Entwickelt von Sabri Ramadan, VISPIRON SYSTEMS GmbH")
        
    def adjust_scaling(self):
        """ Anpassen der Fenster-Skalierung """
        scale_factor = radio_value.get() / 100
        # Korrekturfaktor - je größer die Skalierung desto größer der Zuwachs an Breite / desto niedriger der Zuwachs an Höhe
        user_screen_width05 = int(window.winfo_screenwidth() * (scale_factor/3)) + int((scale_factor - (1/scale_factor))*100)
        user_screen_height05 = int(window.winfo_screenheight() * scale_factor*0.5) - int((scale_factor - (1/scale_factor))*100)
        user_screen_offsetx = int((window.winfo_screenwidth() - user_screen_width05) / 2)
        # Y-Offset soll kleiner werden je größer die Skalierung als Kompensation für die größere Taskleiste
        user_screen_offsety = int((window.winfo_screenheight() - user_screen_height05) / 2) - int((scale_factor - (1/scale_factor))*30)
        sizestring = "" + str(user_screen_width05) + "x" + str(user_screen_height05) + "+" + str(user_screen_offsetx) + "+" + str(user_screen_offsety)
        window.geometry(sizestring)
        

    def import_config(self):
        """ Optionsdatei importieren/erstellen """
        global bt_nr_column
        global startrow
        global startcolumn
        global bt_col_input
        global bt_row_input
        global compare_col_input
        
        global chkbox1_value
        global chkbox2_value
        global radio_value
        
        config = configparser.ConfigParser()
        
        try:
            config.read('ksp_optionen.ini')
            try:
                # Spalte in der die BT-Nummern stehen. Standard: Spalte K
                bt_nr_column = config.get('Startparameter', 'bt_nr_column')
                # Reihe ab der das Programm die Zellen vergleichen soll - erste relevante Reihe unter den Überschriften
                startrow = config.getint('Startparameter', 'startrow')
                # Spalten ab der das Programm die Zellen vergleichen soll
                startcolumn = config.get('Startparameter', 'startcolumn')
                # Excel-Datei nach Ausführung öffnen ja/nein
                chkbox1_value.set(config.getboolean('Interface', 'open_xlsx'))
                # SAs in Strings umwandeln ja/nein
                chkbox2_value.set(config.getboolean('Interface', 'sa_strings'))
                # Skalierung
                radio_value.set(config.getint('Interface', 'scaling'))
                if not(radio_value.get() == 100 or radio_value.get() == 125 or radio_value.get() == 150 or radio_value.get() == 175):
                    radio_value.set(100)
                self.log_message("Optionsdatei gefunden. Parameter aus 'ksp_optionen.ini' eingelesen.")
            except (ValueError, configparser.NoOptionError):
                # Fehler tritt auf wenn keine Zahlenwerte für die Reihen/Spalten angegeben werden
                showerror(title = "Fehler: Ungültige Werte", message = "Ungültige Werte in Optionsdatei." + "\n" + "Optionsdatei wird gelöscht und neu erstellt.")
                os.remove("ksp_optionen.ini")
                # Programm neu starten
                os.execl(sys.executable, sys.executable, *sys.argv)
        except configparser.NoSectionError:
            # Wenn keine Optionsdatei gefunden, wird hier eine erstellt mit Standardwerten
            self.log_message("Optionsdatei nicht gefunden. Erstelle Standarddatei 'ksp_optionen.ini'.")
            # Standardwerte BT-Spalte = K, Startreihe = 3, Startspalte = AW, öffne Excel-Datei nach Programmdurchlauf: ja
            config.add_section('Startparameter')
            config.set('Startparameter', 'bt_nr_column', 'K')
            config.set('Startparameter', 'startrow', '3')
            config.set('Startparameter', 'startcolumn', 'AW')
            config.add_section('Interface')
            config.set('Interface', 'open_xlsx', 'True')
            config.set('Interface', 'sa_strings', 'False')
            config.set('Interface', 'scaling', '100')

            with open('ksp_optionen.ini', 'w') as configfile:
                config.write(configfile)
            
            # Standardwerte vergeben
            bt_nr_column = "K"
            startrow = 3
            startcolumn = "AW"
            radio_value.set(100)
            
        # Textfelder befüllen
        bt_col_input.insert(0,bt_nr_column)
        bt_row_input.insert(0,startrow)
        compare_col_input.insert(0,startcolumn)





class ThreadedClient:
    """
    Launch the main part of the GUI and the worker thread. periodicCall and
    endApplication could reside in the GUI part, but putting them here
    means that you have all the thread controls in a single place.
    """
    def __init__(self, master):
        """
        Start the GUI and the asynchronous threads. We are in the main
        (original) thread of the application, which will later be used by
        the GUI as well. We spawn a new thread for the worker (I/O).
        """
        
        self.master = master

        # Create the queue
        self.queue = queue.Queue()

        # Set up the GUI part
        self.gui = GuiPart(master, self.queue, self.endApplication)

    def periodicCall(self):
        """
        Check every 200 ms if there is something new in the queue.
        """
        self.gui.processIncoming()
        if not self.running:
            # Methode beenden
            #print("beende periodicCall")
            return
        elif self.running == 2:
            #print("kill program")
            sys.exit()
            
        self.master.after(200, self.periodicCall)

    def workerThread1(self):
        """
        This is where we handle the asynchronous I/O. For example, it may be
        a 'select(  )'. One important thing to remember is that the thread has
        to yield control pretty regularly, by select or otherwise.
        """
        try:
            # Verarbeite Excel-Datei
            global workbook
            global bt_nr_column
            global startrow
            global startcolumn
            
            global bt_col_input
            global bt_row_input
            global compare_col_input
            
            wb_title = str(workbook).split("/")[-1]
            if workbook:
                try:
                    wb = load_workbook(workbook)
                    self.queue.put("Exceldatei gefunden. Name: " + wb_title)
                except IOError:
                    self.queue.put("Exceldatei nicht gefunden: " + wb_title)
                    showerror(title = "Fehler: Datei nicht gefunden", message = "IOError: Excel-Datei nicht gefunden." + "\n" + "Bitte erneut versuchen.")
                    self.queue.put("progress_reset")
                    self.running = 0
                    return
                
                # Datum und Uhrzeit auslesen
                d = datetime.now()
                time = d.strftime("%B %d %Y %I.%M%p")
                
                # Textfeld BT-Nummer-Spalte einlesen
                try:
                    bt_nr_column = column_index_from_string(bt_col_input.get().strip())
                except ValueError:
                    self.queue.put("Ungültiger Wert für BT-Nummern Spalte.")
                    showerror(title = "Fehler: Ungültiger Wert", message = "Ungültiger Wert für BT-Nummern Spalte.")
                    self.queue.put("btn_enable")
                    self.queue.put("progress_reset")
                    self.running = 0
                    return
                
                # Textfeld BT-Nummer-Reihe einlesen
                if bt_row_input.get().strip().isdigit():
                    if 1 <= int(bt_row_input.get().strip()) <= 18278:
                        startrow = int(bt_row_input.get().strip())
                    else:
                        self.queue.put("Ungültiger Wert für BT-Nummern Reihe.")
                        showerror(title = "Fehler: Ungültiger Wert", message = "Wert für BT-Nummern Reihe ist außerhalb gültiger Range.")
                        self.queue.put("btn_enable")
                        self.queue.put("progress_reset")
                        self.running = 0
                        return
                else:
                    self.queue.put("Ungültiger Wert für BT-Nummern Reihe.")
                    showerror(title = "Fehler: Ungültiger Wert", message = "Wert für BT-Nummern Reihe ist keine Zahl!")
                    self.queue.put("btn_enable")
                    self.queue.put("progress_reset")
                    self.running = 0
                    return
                
                
                # Textfeld Vergleichs-Startspalte einlesen
                try:
                    startcolumn = column_index_from_string(compare_col_input.get().strip())
                except ValueError:
                    self.queue.put("Ungültiger Wert für Vergleichs-Spalte.")
                    showerror(title = "Fehler: Ungültiger Wert", message = "Ungültiger Wert für Vergleichs-Spalte.")
                    self.queue.put("btn_enable")
                    self.queue.put("progress_reset")
                    self.running = 0
                    return
                
                # progressbar 2
                self.queue.put("progress")
                
                # Wähle aktives Arbeitsblatt aus
                sheet = wb.active
                # Range beschrifteter Zellen auswählen
                self.queue.put("Minimum row: {0}".format(sheet.min_row))
                self.queue.put("Maximum row: {0}".format(sheet.max_row))
                self.queue.put("Minimum column: {0}".format(sheet.min_column))
                self.queue.put("Maximum column: {0}".format(sheet.max_column))
                self.queue.put("Wird verarbeitet. Bitte warten.")
                self.queue.put("")
                
                # progressbar 3
                self.queue.put("progress")
                
                # Neues Arbeitsblatt erstellen durch Kopie mit Titel "Check" und Datum + Uhrzeit
                # Achtung: Wenn Name für Arbeitsblatt 31 Zeichen oder mehr hat wird Datei beschädigt
                ws_out = wb.copy_worksheet(sheet)
                ws_out.title = "Check " + str(time)                
                    
                # Zoom-Stufe angleichen
                ws_out.sheet_view.zoomScale = sheet.sheet_view.zoomScale
                
                # progressbar 4
                self.queue.put("progress")

                # Liste für alle BT-Nummern erstellen - anhand derer werden die einzelnen KSPs zusammengefasst. Jeder KSP hat genau 1 BT-Nummer
                bt_numbers = []
                # Spalte K mit Index 11 ist BT-Nummer - Iteriere über alle Reihen ab Reihe 3
                for i in range(startrow, sheet.max_row+1):
                    curr_cell = ws_out.cell(row=i, column=bt_nr_column)
                    bt_numbers.append(curr_cell.value)
                    self.queue.put("BT-Nummer: " + str(bt_numbers[i-startrow]))
                    
                # progressbar 5
                self.queue.put("progress")
                
                # Über alle Spalten ab angegebener Spalte und über alle Reihen vergleichen
                # In comparelist kommen alle Werte einer konkreten Spalte von einer BT-Nummer - danach wird diese wieder geleert und für die nächste BT-Nummer befüllt
                comparelist = []
                # Loop über Spalten: angegebene Start-Spalte bis zum Ende des Dokuments
                for y in range(startcolumn, sheet.max_column+1):
                    # Loop über Reihen: ab Reihe 3 bis zum Ende des Dokuments - hier wird der Index der BT-Nummern verwendet bei Zugriff auf Reihen um +3 ergänzt
                    for j in range(len(bt_numbers)):
                        
                        # Wenn SAs in Strings umwandeln ausgewählt
                        if chkbox2_value.get():
                            if str(ws_out.cell(row=j+startrow, column=y).value).strip() == "+" and len(str(ws_out.cell(row=startrow-1, column=y).value).strip()) > 5:
                                # Ändere "+" in SA um aus Spaltenüberschrift
                                ws_out.cell(row=j+startrow, column=y).value = str(ws_out.cell(row=startrow-1, column=y).value).strip()[:5]
                        
                        # Check ob Ende des Dokuments erreicht
                        if (j+1) == len(bt_numbers):
                            comparelist.append(ws_out.cell(row=j+startrow, column=y).value)
                            # Prüfe ob alle Werte gleich sind: Dann ist Größe des Sets = 1 (Duplikate werden entfernt)
                            if len(set(comparelist)) == 1:
                                for x in range(len(comparelist)):
                                    # Alle sind gleich - grün einfärben
                                    ws_out.cell(row=j-x+startrow, column=y).fill = PatternFill(fgColor="000FF000", fill_type = "solid")
                            else:
                                for x in range(len(comparelist)):
                                    # Unterschiedliche Werte in den Zellen - rot einfärben
                                    ws_out.cell(row=j-x+startrow, column=y).fill = PatternFill(fgColor="00FF0000", fill_type = "solid")
                            # Vergleichsliste leeren
                            comparelist.clear()
                        else:
                            # Prüfe ob folgende BT-Nummer mit aktueller übereinstimmt
                            if bt_numbers[j] == bt_numbers[j+1]:
                                comparelist.append(ws_out.cell(row=j+startrow, column=y).value)
                            # Letzter Eintrag für diese BT-Nummer erreicht - jetzt wird diese Sektion eingefärbt
                            else:
                                comparelist.append(ws_out.cell(row=j+startrow, column=y).value)
                                # Prüfe ob alle Werte gleich sind: Dann ist Größe des Sets = 1 (Duplikate werden entfernt)
                                if len(set(comparelist)) == 1:
                                    for x in range(len(comparelist)):
                                        # Alle sind gleich - grün einfärben
                                        ws_out.cell(row=j-x+startrow, column=y).fill = PatternFill(fgColor="000FF000", fill_type = "solid")
                                else:
                                    for x in range(len(comparelist)):
                                        # Unterschiedliche Werte in den Zellen - rot einfärben
                                        ws_out.cell(row=j-x+startrow, column=y).fill = PatternFill(fgColor="00FF0000", fill_type = "solid")
                                # Vergleichsliste leeren
                                comparelist.clear()
                
                # progressbar 6
                self.queue.put("")
                self.queue.put("progress")
                
                try:               
                    wb.save(workbook)
                    self.queue.put("Check erfolgreich durchgeführt.")
                    # progressbar 7
                    self.queue.put("progress")
                    
                    # Öffne Excel wenn Checkbox checked
                    if chkbox1_value.get():
                        # windows only
                        # file:// sonst Problem bei Netzwerk-Locations
                        os.startfile("file://" + workbook)
                    
                    # --- CONFIG
                    # Nach Ausführung schreibe Inhalt der Textfelder + Status der Checkbox in config        
                    config = configparser.ConfigParser()
                    config.read('ksp_optionen.ini')
                    
                    config.set('Startparameter', 'bt_nr_column', bt_col_input.get().strip())
                    config.set('Startparameter', 'startrow', bt_row_input.get().strip())
                    config.set('Startparameter', 'startcolumn', compare_col_input.get().strip())
                    config.set('Interface', 'open_xlsx', str(chkbox1_value.get()))
                    config.set('Interface', 'sa_strings', str(chkbox2_value.get()))
                    config.set('Interface', 'scaling', str(radio_value.get()))
                            
                    with open('ksp_optionen.ini', 'w') as configfile:
                        config.write(configfile)
                    # --- // CONFIG
                    
                    self.running = 0
                    
                    
                # Error tritt auf wenn Excel-Datei noch geöffnet ist
                except PermissionError:
                    self.queue.put("Fehler: Zugriff verweigert auf " + str(workbook))
                    showerror(title = "Fehler: Zugriff verweigert", message = "Zugriff auf Excel-Datei verweigert." + "\n" + "Bitte sicherstellen, dass Datei nicht geöffnet ist.")
                    self.queue.put("btn_enable")
                    self.queue.put("progress_reset")
                    self.running = 0
                    return
                
            else:
                self.queue.put("Exceldatei nicht gefunden: " + wb_title)
                showerror(title = "Fehler: Datei nicht gefunden", message = "Excel-Datei nicht gefunden." + "\n" + "Bitte erneut versuchen.")
                self.queue.put("progress_reset")
                self.running = 0
                return
            
            
            self.queue.put("btn_enable")
            
        except Exception as e:
            showerror(title = "Unerwarteter Fehler", message = "Unerwarteter Fehler aufgetreten:" + "\n" + repr(e) + "\n" + "\n" + "Programm wird geschlossen.")
            self.running = 2
        
    def endApplication(self):
        self.running = 0
        # temporäre Lösung ?
        sys.exit()



# -----------------------------------------------------------------------------------------------------



# Create & Configure root
# Base64 Icon String
icon = b'iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAIAAAD/gAIDAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIsSURBVHhe7dVJWQNBFEXhBBEIADsMLrCABuQAKhCBAUzACfRHkk71XMObzuLVXf+b2t+83u0k9fT40S0BPX9/deu3q+6NLupJUWClu5SiwEqUlKLA6jckRYF11ogUBdaxcSkKrK5JqZf9dWAdmiPFDay5UuQda74UucZaJEV+sZZKkVOsFVLkEWudFLnDWi1FvrC2SJEjrI1S5AVruxS5wMoiRfaxckmRcayMUmQZK68UmcXKLkU2sUpIkUGsQlJkDaucFJnCKipFdrBKS5ERrApSZAGrjhSpx6omRbqxakqRYqzKUqQVq74UqcRqIkX6sFpJkTKshlKkCautFKnBai5FOrAkSJECLCFSJB1LjhSJxhIlRXKxpEmRUCyBUiQRS6YUicMSK0WysD4f3rs1UEMpEoQlXIqkYMmXIhFYKqSoPZYWKWqMpUiKWmLpkqJmWOqkqA2WRilqgKVUimpj6ZWiqliTUrdv990SWT0s7VJUCcuAFNXAsiFFxbHMSFFZLEtSVBDLmBSVwrInRUWwTEpRfiyrUpQZy7AU5cSyLUXZsMxLUR4sD1KUAcuJFG3F8iNFm7BcSdF6LG9StBLLoRStwfIpRYux3ErRMizPUrQAy7kUzcUKKZqFFVJ/TWOF1H8TWCF12hhWSPUaxAqpy9JYIZUsgRVSQ/WxQmqkM6yQGu+IFVKTdVghNacDVkjN7Cqk5tf/DXuF1GljWCHVaxArpC5LY4VUot3uBxtoHgZ6Pj1XAAAAAElFTkSuQmCC'
window = tkinter.Tk()

imgicon = tkinter.PhotoImage(data=icon)
window.tk.call('wm', 'iconphoto', window._w, imgicon)

client = ThreadedClient(window)

# Starte Fenster
window.mainloop()

