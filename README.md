# KSP Vehicle Configuration Checker

Tool for the automated comparison of vehicle configurations in Excel-exports. It will mark correct configurations green and incorrect ones red.

It will create a complete copy of the active worksheet and mark down the configurations inside the copy.

### Language used

Python 3.7

### Required Libraries

OpenPyXL -  A Python library to read/write Excel xlsx/xlsm files

PyInstaller (optional) - Library to create windows executable for systems that don't have Python installed

## Keywords

* UI
* Threading
* Data computation


![Screenshot1](https://i.ibb.co/Ry5LgM8/ksp-checker1.png)
![Screenshot2](https://i.ibb.co/bKJqJRg/ksp-checker2.png)
![Result](https://i.ibb.co/1TcPNv2/ksp-checker-example.png)

